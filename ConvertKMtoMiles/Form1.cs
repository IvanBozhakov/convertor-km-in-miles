﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConvertKMtoMiles
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double lpkm = Convert.ToDouble(textBox1.Text);
            double result = 235.21458333 / lpkm;
            label2.Text = result.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            double mpg = Convert.ToDouble(textBox2.Text);
            double result = 235.21458333 / mpg;
            label5.Text = result.ToString();
        }
    }
}
